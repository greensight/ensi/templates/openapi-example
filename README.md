# Пример описания API в спецификации openapi 3

Пример удовлетворяет требованиям
- [Ensi Api Design Guide](https://ensi-platform.gitlab.io/docs/guid/api)
- [laravel-openapi-client-generator](https://github.com/greensight/laravel-openapi-client-generator/blob/master/docs/api_schema_requirements.md)
- [laravel-openapi-server-generator](https://github.com/greensight/laravel-openapi-server-generator/blob/master/docs/api_schema_requirements.md)

Название директории `customers` соответствует названию модуля API в вашем сервисе. 

Для лучшего понимания иерархии схем для ресурса полезна [визуализация в Miro](https://miro.com/app/board/o9J_lPKk7yY=/) 

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
